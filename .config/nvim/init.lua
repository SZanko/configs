local execute = vim.api.nvim_command
local install_path = vim.fn.stdpath("data") .. "/site/pack/paqs/start/paq-nvim"

if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  execute("!git clone https://github.com/savq/paq-nvim.git " .. install_path)
end

require "paq" {
  "savq/paq-nvim", -- Let Paq manage itself
  "neovim/nvim-lspconfig", -- Mind the semi-colons
  --autocompletion CMP Plugins
  "hrsh7th/nvim-cmp",
  "hrsh7th/cmp-nvim-lsp",
  "hrsh7th/cmp-nvim-lua",
  "hrsh7th/cmp-buffer",
  "hrsh7th/cmp-path",
  "hrsh7th/cmp-cmdline",
  "hrsh7th/cmp-calc",
  --"VonHeikemen/lsp-zero.nvim",

  "rcarriga/cmp-dap",
  "kdheepak/cmp-latex-symbols",
  "hrsh7th/cmp-emoji",
  "saadparwaiz1/cmp_luasnip",
  "L3MON4D3/LuaSnip",
  "rafamadriz/friendly-snippets",
  "williamboman/mason.nvim",
  "williamboman/mason-lspconfig.nvim",
  "preservim/nerdtree",
  "nvim-telescope/telescope.nvim",
  "kylechui/nvim-surround",
  "nvim-lua/popup.nvim",
  "kyazdani42/nvim-web-devicons",
  "hoob3rt/lualine.nvim",
  --"norcalli/snippets.nvim",
  "flazz/vim-colorschemes",
  "sheerun/vim-polyglot",
  "airblade/vim-gitgutter",
  "nvim-lua/plenary.nvim",
  "ruifm/gitlinker.nvim",
  "Yggdroot/indentLine",
  "mfussenegger/nvim-lint",
  "nvim-neotest/nvim-nio",
  "mfussenegger/nvim-dap",
  "rcarriga/nvim-dap-ui",
  "mfussenegger/nvim-dap-python",
  "mfussenegger/nvim-jdtls",
  "theHamsta/nvim-dap-virtual-text",
  "Olical/conjure",
  --"sbdchd/neoformat";
  "mhartington/formatter.nvim",
  "akinsho/flutter-tools.nvim",
  --"phaazon/hop.nvim";
  "lewis6991/gitsigns.nvim",
  "Olical/conjure",
  "windwp/nvim-ts-autotag",
  {"nvim-treesitter/nvim-treesitter", run = ":TSUpdate"},
  "norcalli/nvim-colorizer.lua",
  -- Latex
  --"lervag/vimtex",
  "jakewvincent/texmagic.nvim",
  "xuhdev/vim-latex-live-preview",
  --"brymer-meneses/grammar-guard.nvim"
  "simrat39/rust-tools.nvim",
  "sainnhe/everforest"
}

require "gitlinker".setup()
require "nvim-ts-autotag".setup()
require "flutter-tools".setup()
require "gitsigns".setup()
require "nvim-surround".setup()
require "debug-config"
require "keymappings"
require "settings"
require "colorizer".setup()
require "formatting"
require "autocompletion/completion-nvim"
require "autocompletion/nvim-cmp"
require "lsp-config"
require "snippets-config"
require "treesitter"
require "latex"
