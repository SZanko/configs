if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))
Plug 'tpope/vim-surround'
Plug 'preservim/nerdtree'
Plug 'junegunn/goyo.vim'
Plug 'jreybert/vimagit'
"Plug 'lukesmithxyz/vimling'
"Plug 'vimwiki/vimwiki'
Plug 'bling/vim-airline'
Plug 'tpope/vim-commentary'
Plug 'kovetskiy/sxhkd-vim'
Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
Plug 'vim-scripts/peaksea'
Plug 'ycm-core/YouCompleteMe',{'do': 'python install.py --all', 'for':['tex','java','typescript','vim','nvim','python','py','markdown','c','make','cpp','cxx','h','hpp','jproperties','kotlin','bash','shell','docker','groovy','haskell', 'angular']}
Plug 'ycm-core/lsp-examples', {'for':['kotlin','bash','shell','docker','groovy','haskell', 'angular','scala','rust']}
Plug 'ternjs/tern_for_vim'
Plug 'sheerun/vim-polyglot'
"Plug 'dart-lang/dart-vim-plugin', {'for':'dart'}
Plug 'thosakwe/vim-flutter', {'for':'dart'}
Plug 'neoclide/coc.nvim', {'branch':'release','for':['dart','svelte']}
Plug 'codechips/coc-svelte', {'do': 'npm install', 'for':['svelte']}
Plug 'iamcco/markdown-preview.nvim' ,{'do': 'cd app & npm install','for':'markdown'}
Plug 'udalov/kotlin-vim', {'for':'kotlin'}
"Plug 'neovim/nvim-lspconfig'
Plug 'nvie/vim-flake8', {'for':'python'}
Plug 'airblade/vim-gitgutter'
Plug 'luochen1990/rainbow', {'for':['typescript','dart']}
Plug 'dense-analysis/ale'
Plug 'rust-lang/rust.vim', {'for':'rust'}
Plug 'Yggdroot/indentLine'
Plug 'flazz/vim-colorschemes'
"Plug 'skammer/vim-css-color'
Plug 'mattn/emmet-vim', {'for':['css','html']}
Plug 'tpope/vim-fugitive'
Plug 'shumphrey/fugitive-gitlab.vim'
Plug 'rhysd/vim-grammarous'
Plug 'puremourning/vimspector'
call plug#end()

set number
set encoding=utf-8
syntax on
setlocal spell
set spelllang=de,en_gb
set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab
set title
set lazyredraw
if ! has("gui_running")
	set t_Co=256
endif
set background=dark
"colors PaperColor
colors peaksea
"Removed the green sidecolumn
highlight SignColumn ctermbg=None

"set clipboard+=unnamedplus
"vnoremap <C-y> "+y
"map <C-p> "+p

"nnoremap <A-h> <C-w>h
"nnoremap <A-j> <C-w>j
"nnoremap <A-k> <C-w>k
"nnoremap <A-l> <C-w>l

noremap <silent> <C-h> :vertical resize -3 <CR>
noremap <silent> <S-C-j> :resize +3 <CR>
noremap <silent> <S-C-k> :resize -3 <CR>
noremap <silent> <C-l> :vertical resize +3 <CR>

nnoremap ,gs :Git status <CR>
nnoremap ,gc :Git commit -a -s <CR>
nnoremap ,gr :Git rebase -i <CR>
nnoremap ,gl :Git log <CR>
nnoremap ,gm :Git mergetool <CR>
nnoremap ,gd :Git difftool <CR>
nnoremap ,gb :GBrowse <CR>
nnoremap ,t :tabs<cr>:tabnext<home>

nnoremap ,html :-1read $HOME/Vorlagen/programmieren/web/html/basictemplate.html<CR>5jwf>a
nnoremap ,py :-1read $HOME/Vorlagen/programmieren/python/basicpython.py<CR>GA<TAB>
nnoremap ,beg :-1read $HOME/Vorlagen/latex/begin_end.tex<CR>2wei
nnoremap ,java :-1read $HOME/Vorlagen/programmieren/java/basic_class.java<CR>

nnoremap ,, <Esc>/<++><Enter>"_c4l
nnoremap <A-s> :vs<CR>
nnoremap <A-q> :tabclose<CR>
nnoremap <A-n> :NERDTreeToggle<CR>
inoremap [ []<Esc>i
inoremap { {}<Esc>i
inoremap ( ()<Esc>i
inoremap " ""<Esc>i
inoremap ' ''<Esc>i
inoremap < <><Esc>i

let g:vimspector_enable_mappings = 'HUMAN'
let g:livepreview_previewer = 'zathura'
let g:livepreview_use_biber = 1
let g:livepreview_cursorhold_recompile = 0
let g:lsc_auto_map= v:true
let g:lsc_server_commands = {'dart': 'dart_language_server'}
let g:rainbow_active = 1
let g:ale_completion_autoimport = 1
let g:ale_fix_on_save = 1
"mypy, javac linter doesn't work
let g:ale_linters = {
			\	'python' : ['flake8','pylint','pyright','isort'],
			\	'css' : ['prettier'],
			\	'java' : ['checkstyle','eclipselsp','javalsp','pmd','google_java_format']
			\}

let g:ale_fixers ={
			\	'*': ['remove_trailing_lines', 'trim_whitespace'],
			\	'python' : ['autopep8','autoimport'],
			\	'c' : ['clang-format'],
			\	'java' : ['google_java_format'],
			\	'dart' : ['darfmt'],
			\	'html' : ['prettier'],
			\	'css' : ['prettier'],
			\	'typescript' : ['eslint','prettier'],
			\	'svelte' : ['prettier']
			\}
let g:airline#extensions#ale#enabled = 1
let g:vim_svelte_plugin_use_typescript = 1
let g:vim_svelte_plugin_use_less = 1
let g:vim_svelte_plugin_use_sass = 1

fun! GoYCM()
	let g:ycm_global_ycm_extra_conf = '~/.config/nvim/plugged/YouCompleteMe/third_party/ycmd/.ycm_extra_conf.py'
	nnoremap <buffer> <silent> <A-g> :YcmCompleter GoTo<CR>
	nnoremap <buffer> <silent> <A-e> :YcmCompleter GoToReferences<CR>
	nnoremap <buffer> <silent> <A-r> :YcmCompleter RefactorRename<SPACE>
	nnoremap <buffer> <silent> <A-f> :YcmCompleter FixIt<CR>
endfun

function! s:check_back_space() abort
	let col = col('.') - 1
	return !col || getline('.')[col - 1]  =~# '\s'
endfunction

fun! GoCoc()
	inoremap <silent><expr> <TAB>
				\ pumvisible() ? "\<C-n>" :
				\ <SID>check_back_space() ? "\<TAB>" :
				\ coc#refresh()
	inoremap <buffer> <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
	inoremap <buffer> <silent><expr> <C-space> coc#refresh()

	" GoTo code navigation.
	nmap <silent> <buffer> <A-g> <Plug>(coc-definition)
	nmap <silent> <buffer> <A-t> <Plug>(coc-type-definition)
	nmap <silent> <buffer> <A-w> <Plug>(coc-implementation)
	nmap <silent> <buffer> <A-e> <Plug>(coc-references)


endfun

fun! CurlyBrackets()
	inoremap { {<CR>}<Esc>ko
endfun

fun! Java()
	let $JAVA_TOOL_OPTIONS='-javaagent:/usr/lib/lombok-common/lombok.jar'
	nnoremap <buffer> <silent> <F3>  :YcmCompleter Format<CR>
	nnoremap ,rc :-1read $HOME/Vorlagen/programmieren/java/example_rest_controller.java<CR>21Gf<dwi
endfun

fun! Html()
	nnoremap <buffer> <F5> :silent update<Bar>silent !firefox %:p &<CR>
	inoremap <buffer> !com <!-- <++> --><Esc>2bvedi
	inoremap <buffer> !DOC <!DOCTYPE html><Esc>
	inoremap <buffer> !div <div > <++> </div><Esc>02ela
	inoremap <buffer> !link <a href=""> <++> </a><Esc>04ehi
	"nnoremap <F5> :silent update<Bar>silent !firefox %:p:s?\(.\{-}/\)\{4}?http://localhost/?<CR>
	inoremap <buffer> < <> <++> </<++>><Esc>kA<Esc>ei
endfun
fun! Latex()
	nnoremap <buffer> <F5> :LLPStartPreview<CR>
endfun

fun! Markdown()
	nnoremap <buffer> <F5> :MarkdownPreview<CR>
	inoremap ( (<Esc>a
	inoremap ' '<Esc>a
endfun

fun! Json()
	nnoremap <buffer> =G :%!python -m json.tool<CR>
endfun

fun! Python()
	nnoremap <buffer> <silent> =G :ALEFix<CR>
endfun


autocmd FileType tex :call Latex()
autocmd FileType dart,svelte :call GoCoc()
autocmd FileType java :call Java()
autocmd FileType typescript,java,text,nvim,vim,markdown,python,jproperties,cpp,cxx,h,c,hpp :call GoYCM()
autocmd FileType html,svelte :call Html()
autocmd FileType dart,cpp,cxx,h,hpp,c,java :call CurlyBrackets()
autocmd FileType markdown :call Markdown()
autocmd FileType rmd map <F5> :!echo<space>"require(rmarkdown);<space>render('<c-r>%')"<space>\|<space>R<space>--vanilla <CR> <CR>
autocmd FileType json :call Json()
autocmd FileType python :call Python()
au VimEnter * silent !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'
"au VimLeave * silent !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Caps_Lock'

"source /home/stefan/.config/nvim/plugged/lsp-examples/vimrc.generated
