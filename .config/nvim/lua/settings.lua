-----------------------------------------------------------
-- Neovim API aliases
-----------------------------------------------------------
local cmd = vim.cmd -- execute Vim commands
local exec = vim.api.nvim_exec -- execute Vimscript
--local map = vim.api.nvim_set_keymap  -- set global keymap
local fn = vim.fn -- call Vim functions
local g = vim.g -- global variables
local o = vim.o -- global options
local b = vim.bo -- buffer-scoped options
local w = vim.wo -- windows-scoped options

-----------------------------------------------------------
-- General
-----------------------------------------------------------
o.syntax = "enable" -- enable syntax highlighting
w.number = true -- show line number
o.showmatch = true -- highlight matching parenthesis
w.foldmethod = "marker" -- enable folding (default 'foldmarker')
o.title = true
o.encoding = "utf-8"
o.spelllang = "de,en_gb"
o.spell = true
--g.mapleader = " "

-----------------------------------------------------------
-- Memory, CPU
-----------------------------------------------------------
o.hidden = true -- enable background buffers
o.history = 500 -- remember n lines in history
o.lazyredraw = true -- faster scrolling
--b.synmaxcol = 240       -- max column for syntax highlight

-----------------------------------------------------------
-- Colorscheme
-----------------------------------------------------------
o.termguicolors = true
o.background = "light"
--cmd([[colorscheme solarized8_light]])    -- set colorscheme
cmd([[colorscheme PaperColor]]) -- set colorscheme
--cmd([[colorscheme peaksea]])    -- set colorscheme

-----------------------------------------------------------
-- Tabs, indent
-----------------------------------------------------------
b.expandtab = true -- use spaces instead of tabs
b.shiftwidth = 4 -- shift 4 spaces when tab
b.tabstop = 4 -- 1 tab == 4 spaces
b.smartindent = true -- autoindent new lines
b.softtabstop = -1

-----------------------------------------------------------
-- Autocompletion
-----------------------------------------------------------
o.completeopt = "menuone,noselect,noinsert" -- completion options
o.shortmess = "c" -- don't show completion messages
g.completion_matching_strategy_list = {"exact", "substring", "fuzzy", "all"}

-----------------------------------------------------------
-- Status Line
-----------------------------------------------------------
require("lualine").setup {
  options = {
    theme = "papercolor_dark",
    icons_enabled = false
  }
}

-----------------------------------------------------------
-- Formatting
-----------------------------------------------------------
--exec([[
--augroup FormatAutogroup
--  autocmd!
--  autocmd BufWritePre * undojoin | Neoformat
--augroup END
--]], true)
exec([[
augroup FormatAutogroup
  autocmd!
  autocmd BufWritePost *.js,*.rs,*.lua FormatWrite
augroup END
]], true)

-----------------------------------------------------------
-- Latex
-----------------------------------------------------------
g.vimtex_view_method = "zathura"
g.tex_flavor = "latex"
o.conceallevel = 2
g.vimtex_quickfix_enabled = 0

g.livepreview_previewer = "zathura"
g.livepreview_use_biber = 1
g.livepreview_cursorhold_recompile = 0

-----------------------------------------------------------
-- Svelte
-----------------------------------------------------------
g.vim_svelte_plugin_use_typescript = 1
g.vim_svelte_plugin_use_less = 1
g.vim_svelte_plugin_use_sass = 1
-----------------------------------------------------------
-- Deno
-----------------------------------------------------------
g.markdown_fenced_languages = {
  "ts=typescript"
}
