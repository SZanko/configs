local _latex = {}
local U = require "snippets.utils"

local _subs =
  [[
	\subsubsection{${1:subsubsection name}} % (fold)
	\label{ssub:${2:${1/(\w+)(\W+$)?|\W+/${1:?${1:/asciify/downcase}:_}/g}}}
	${0:$TM_SELECTED_TEXT}
	% subsubsection $2 (end)
]]
local _table_acm_ =
  [[
	\begin{table*}
		\caption{$1}
		\label{tab:$2}
		\begin{tabular}{${3:ccl}}
			\toprule
			$4
			a & b & c \\\\
			\midrule
			d & e & f \\\\
			\bottomrule
		\end{tabular}
	\end{table*}
	$0
]]
local _while = [[
	\While{$1}
		\State $0
	\EndWhile
]]
local _part =
  [[
	\part{${1:part name}} % (fold)
	\label{prt:${2:${1/(\w+)(\W+$)?|\W+/${1:?${1:/asciify/downcase}:_}/g}}}
	${0:$TM_SELECTED_TEXT}
	% part $2 (end)
]]
local _table_ref = [[
	${1:Table}~\ref{${2:tab:}}$0
]]
local _sub =
  [[
	\subsection{${1:subsection name}} % (fold)
	\label{sub:${2:${1/(\w+)(\W+$)?|\W+/${1:?${1:/asciify/downcase}:_}/g}}}
	${0:$TM_SELECTED_TEXT}
	% subsection $2 (end)
]]
local _table =
  [[
	\begin{table}
		\begin{small}
			\caption{$1}
			\label{tab:$2}
			\begin{center}
				\begin{tabular}[c]{l|l}
					\hline
					\multicolumn{1}{c|}{\textbf{$3}} & 
					\multicolumn{1}{c}{\textbf{$4}} \\\\
					\hline
					a & b \\\\
					c & d \\\\
					$5
					\hline
				\end{tabular}
			\end{center}
		\end{small}
	\end{table}
	$0
]]
local _desc = [[
	\\begin{description}
		\item[$1] $0
	\\end{description}
]]
local _if = [[
	\If{$1}
	\ElsIf{$2}
	\Else
	\EndIf
]]
local _figure =
  [[
	\begin{figure}
		\begin{small}
			\begin{center}
				\includegraphics[width=0.95\textwidth]{figures/$1}
			\end{center}
			\caption{$3}
			\label{fig:$4}
		\end{small}
	\end{figure}
	$0
]]
local _enumerate = [[
	\begin{enumerate}
		\item $1
	\end{enumerate}
	$0
]]
local _theorem = [[
	\begin{theorem}
		$1
		\begin{displaymath}
			$2
		\end{displaymath}
		$3
	\end{theorem}
	$0
]]
local _mat = [[
	\begin{${1:p/b/v/V/B/small}matrix}
		$0
	\end{${1:p/b/v/V/B/small}matrix}
]]
local __endregion = [[
	%#Endregion
]]
local _subp =
  [[
	\subparagraph{${1:subparagraph name}} % (fold)
	\label{subp:${2:${1/(\w+)(\W+$)?|\W+/${1:?${1:/asciify/downcase}:_}/g}}}
	${0:$TM_SELECTED_TEXT}
	% subparagraph $2 (end)
]]
local _empty = [[
	\null\thispagestyle{empty}
	\newpage
	$0
]]
local _par =
  [[
	\paragraph{${1:paragraph name}} % (fold)
	\label{par:${2:${1/(\w+)(\W+$)?|\W+/${1:?${1:/asciify/downcase}:_}/g}}}
	${0:$TM_SELECTED_TEXT}
	% paragraph $2 (end)
]]
local _cite = [[
	~\cite{$1}$0
]]
local _displaymath = [[
	\begin{displaymath}
		$1
	\end{displaymath}
	$0
]]
local _page = [[
	${1:page}~\pageref{$2}$0
]]
local _math = [[
	\begin{math}
		$1
	\end{math}
	$0
]]
local _compactitem = [[
	\begin{compactitem}
		\item $1
	\end{compactitem}
	$0
]]
local _table_acm =
  [[
	\begin{table}
		\caption{$1}
		\label{tab:$2}
		\begin{tabular}{${3:ccl}}
			\toprule
			$4
			a & b & c \\\\
			\midrule
			d & e & f \\\\
			\bottomrule
		\end{tabular}
	\end{table}
	$0
]]
local _figure_acm_ =
  [[
	\begin{figure*}
		\includegraphics[width=0.45\textwidth]{figures/$1}
		\caption{$2}
		\label{fig:$3}
	\end{figure*}
	$0
]]
local _cha =
  [[
	\chapter{${1:chapter name}} % (fold)
	\label{cha:${2:${1/(\w+)(\W+$)?|\W+/${1:?${1:/asciify/downcase}:_}/g}}}
	${0:$TM_SELECTED_TEXT}
	% chapter $2 (end)
]]
local _figure_acm =
  [[
	\begin{figure}
		\includegraphics[width=0.45\textwidth]{figures/$1}
		\caption{$2}
		\label{fig:$3}
	\end{figure}
	$0
]]
local _item = [[
	\\begin{itemize}
		\item $0
	\\end{itemize}
]]
local _begin =
  [[
	\\begin{${1:env}}
		${1/(enumerate|itemize|list)|(description)|.*/(?1:\item )(?2:\item)/}$0
	\\end{${1:env}}
]]
local _for = [[
	\For{i=0:$1}
		\State $0
	\EndFor
]]
local _tab =
  [[
	\\begin{${1:t}${1/(t)$|(a)$|(.*)/(?1:abular)(?2:rray)/}}{${2:c}}
	$0${2/((?<=[clr])([ |]*(c|l|r)))|./(?1: & )/g}
	\\end{${1:t}${1/(t)$|(a)$|(.*)/(?1:abular)(?2:rray)/}}
]]
local _spl = [[
	\begin{split}
		$0
	\end{split}
]]
local _section_ref = [[
	${1:Section}~\ref{${2:sec:}}$0
]]
local _proof = [[
	\begin{proof}
		$1
		\begin{displaymath}
			$2
		\end{displaymath}
		$3
	\end{proof}
	$0
]]
local _cas = [[
	\begin{cases}
		${1:equation}, &\text{ if }${2:case}\\\\
		$0
	\end{cases}
]]
local _state = [[
	\State $1
]]
local _listing_ref = [[
	${1:Listing}~\ref{${2:lst:}}$0
]]
local _equation = [[
	\begin{equation}
		$2
		\label{eq:$1}
	\end{equation}
	$0
]]
local _gat =
  [[
	\begin{gather`echo $1|grep math|
	sed -e 's/.*math.*/ed/'`}
		$2
	\end{gather`echo $1|grep math|
	sed -e 's/.*math.*/ed/'`}
]]
local _definition =
  [[
	\begin{definition}
		$1
		\begin{displaymath}
			$2
		\end{displaymath}
		$3
	\end{definition}
	$0
]]
local _figure_ref = [[
	${1:Figure}~\ref{${2:fig:}}$0
]]
local _ali =
  [[
	\begin{align`echo $1|grep math|
	sed -e 's/.*math.*/ed/'`}
		$2
	\end{align`echo $1|grep math|
	sed -e 's/.*math.*/ed/'`}
]]
local _algo_ref = [[
	${1:Algorithm}~\ref{${2:algo:}}$0
]]
local __region = [[
	%#Region $0
]]
local _algo =
  [[
	% \usepackage{algorithm,algorithmicx,algpseudocode}
	\begin{algorithm}
		\floatname{algorithm}{${1:Algorithm}}
		\algrenewcommand\algorithmicrequire{\textbf{${2:Input: }}}
		\algrenewcommand\algorithmicensure{\textbf{${3:Output: }}}
		\caption{$4}
		\label{alg:$5}
		\begin{algorithmic}[1]
			\Require \$input\$
			\Ensure \$output\$
			$6
			\State \textbf{return} \$state\$
		\end{algorithmic}
	\end{algorithm}
	$0
]]
function _latex.snippets()
  return {
    subs = _subs,
    table_acm_ = _table_acm_,
    ["while "] = _while,
    part = _part,
    table_ref = _table_ref,
    sub = _sub,
    table = _table,
    desc = _desc,
    ["if"] = _if,
    figure = _figure,
    enumerate = _enumerate,
    theorem = _theorem,
    mat = _mat,
    endregion = __endregion,
    subp = _subp,
    empty = _empty,
    par = _par,
    cite = _cite,
    displaymath = _displaymath,
    page = _page,
    math = _math,
    compactitem = _compactitem,
    table_acm = _table_acm,
    figure_acm_ = _figure_acm_,
    cha = _cha,
    figure_acm = _figure_acm,
    item = _item,
    begin = _begin,
    ["for"] = _for,
    tab = _tab,
    spl = _spl,
    section_ref = _section_ref,
    proof = _proof,
    cas = _cas,
    state = _state,
    listing_ref = _listing_ref,
    equation = _equation,
    gat = _gat,
    definition = _definition,
    figure_ref = _figure_ref,
    ali = _ali,
    algo_ref = _algo_ref,
    region = __region,
    algo = _algo
  }
end

return _latex
