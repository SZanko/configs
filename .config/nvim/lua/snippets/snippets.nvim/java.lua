local _java = {}
local U = require'snippets.utils'

local _main = U.match_indentation [[
public static void main(String args[]) {
  ${0}
}
]]

local _print = [[
System.out.println(${0});
]]

local _class = [[
package <++>;

/**
 * ${2:Description}
 * @author	Stefan Zanko
 * @version	1.0
 * @since	${=os.date('%Y-%D-%m')}	
 */

public ${2:class} ${1} ${3:extends} {
	${0}

}
]]

local _springboot_controller_rest = [[
package <++>;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * ${3:Description}
 * @author  Stefan Zanko
 * @version 1.0
 * @since	${=os.date('%Y-%D-%m')}
 */

@RestController
@RequestMapping(path="/api/${2}")
public class ${1}Controller {

	/**
	 * The Repository which is used by this Controller
	 */
	@Autowired
	private ${1}Repository ${2}Repository;

	/**
	 * Return all ${1}s in the Repository
	 *
	 * @return List<${1}> as Json
	 */
	@GetMapping
	@Async
	public @ResponseBody Iterable<${1}>
	getAll${1}s(){
		return ${1}Repository.findAll();
	}

	/**
	 * Gets one ${1} by their Id
	 * at the url /api/${2}/{id}
	 *
	 * @param id of the wanted Patient
	 * @return Patient with the given id as a String
	 */
	@GetMapping("/{id}")
	@Async
	public ResponseEntity<${1}>
	get${1}(@PathVariable String id){
		${1} ${2} = ${2}Repository.findById(id).get();

		if(${2} == null) {

			return ResponseEntity.notFound().build();

		}

		return ResponseEntity.ok().body(${2}); 
	}

	/**
	 * Saves one ${1} to the Database
	 *
	 * @param ${2} the full ${1} as a Json
	 * @return Saved ${1}
	 */
	@PostMapping
	@Async
	public ResponseEntity<${1}>
	new${1}(@RequestBody ${1} ${2}){
        ${2}.setId(null);
        ${1} saved = ${2}Repository.save(${2});
		return ResponseEntity.created(URI.create("${2}" + saved.getId())).body(saved);
	}

	/**
	 * Updates a ${1} by their Id
	 * at the url /api/${2}/{id}
	 *
	 * @param   new${2} the full ${1} as Json
	 * @param   id the Id as a String of the ${1} which will be updated
	 * @return  the updated ${1} 
	 */
	@PutMapping("/{id}")
	@Async
	public ${1}
	update${1}(@RequestBody ${1} new${2}, @PathVariable String id){
		return ${2}Repository.findById(id).map(
				${2} ->{
                    ${0}
					${2}.setProperty() //edit this Line
					return ${2}Repository.save(${2});
				}
				).orElseGet(()->{
			${2}.setId(id);

			return ${2}Repository.save(newpatient);
		});
	}

	/**
	 * Deletes a ${1} by their Id
	 * at the url /api/${2}/{id}
	 *
	 * @param id the Id as a String of the ${1} which will be deleted
	 */
	@DeleteMapping("/{id}")
	@Async
	public void
	delete${1}(@PathVariable String id){
		${2}Repository.deleteById(id);
	}
}
]]

function _java.snippets()
  return {
    main = _main,
    ["print"]= _print,
    class = _class,
    springboot_controller_rest = _springboot_controller_rest
  }
end

return _java
