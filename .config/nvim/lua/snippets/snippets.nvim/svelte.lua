local _svelte = {}
local U = require'snippets.utils'

local _s_on_event_modifiers = [[
	on:${1:eventname}|${2|preventDefault,stopPropagation,passive,capture,once|}={${3:handler}}
]]
local _s_self = [[
	<svelte:self />
]]
local _s_options = [[
	<svelte:options ${1|immutable,accessors,namespace,tag|}={${2:value}}/>
]]
local _s_await_then_block = [[
	{#await ${1:promise}}
		<!-- promise is pending -->
	{:then ${2:value}}
		<!-- promise was fulfilled -->
	{/await}
]]
local _s_on_event = [[
	on:${1:eventname}={${2:handler}}
]]
local _s_body = [[
	<svelte:body />
]]
local _s_bind_property = [[
	bind:${1:property}={${2:variable}}
]]
local _s_bind_group = [[
	bind:group={${1:variable}}
]]
local _s_bind_audio = [[
	<audio
	src={${1:clip}}
	bind:${2:duration}
	bind:${3:buffered}
	bind:${4:played}
	bind:${5:seekable}
	bind:${6:seeking}
	bind:${7:ended}
	bind:${8:currentTime}
	bind:${9:playbackRate}
	bind:${10:paused}
	bind:${11:volume}
	bind:${12:muted}
	></audio>
]]
local _s_expression_html = [[
	{@html ${1:expression}}
]]
local _s_head = [[
	<svelte:head>
		${1:<!-- head content -->}
	</svelte:head>
]]
local _s_bind_block_level = [[
	bind:${1|clientWidth,clientHeight,offsetWidth,offsetHeight|}={${2:variable}}
]]
local _s_bind_media_elements = [[
	bind:${1|duration,buffered,played,seekable,seeking,ended,currentTime,playbackRate,paused,volume,muted,videoWidth,videoHeight|}
]]
local _s_window_bind = [[
	bind:${1|innerWidth,innerHeight,outerWidth,outerHeight,scrollX,scrollY,online|}={${2:variable}}
]]
local _s_transition_params = [[
	${1|transition,in,out|}:${2:name}={${3:params}}
]]
local _s_else_block = [[
	{:else}
		${1: <!-- else content here -->}
]]
local _s_script_context = [[
	<script context="module">
		${1:// your script goes here}
	</script>
]]
local _s_each_block = [[
	{#each ${1:items} as ${2:item}}
		${3: <!-- content here -->}
	{/each}
]]
local _s_component = [[
	<svelte:component this={${1:component}} />
]]
local _s_style = [[
	<style>
		${1:/* your styles go here */}
	</style>
]]
local _s_component_format = [[
	<script>
		${1:// your script goes here}
	</script>
	
	<style>
		${2:/* your styles go here */}
	</style>
	
	${3:<!-- markup (zero or more items) goes here -->}
]]
local _s_expression_debug = [[
	{@debug ${1:var1}${2:,var2}}
]]
local _s_self_prop = [[
	<svelte:self ${1:prop}={${2:value}} />
]]
local _s_slot_prop = [[
	<slot ${1:prop}={${2:value}}>${3:<!-- optional fallback -->}</slot>
]]
local _s_class = [[
	class:${1:name}={${2:condition}}
]]
local _s_slot_name = [[
	<slot name="${1:x}">${2:<!-- optional fallback -->}</slot>
]]
local _s_animate = [[
	animate:${1:name}={${2:params}}
]]
local _s_script = [[
	<script>
		${1:// your script goes here}
	</script>
]]
local _s_slot = [[
	<slot>${1:<!-- optional fallback -->}</slot>
]]
local _s_await_catch_block = [[
	{#await ${1:promise}}
		<!-- ${1:promise} is pending -->
	{:then ${value}}
		<!-- ${1:promise} was fulfilled -->
	{:catch error}
		<!-- ${1:promise} was rejected -->
	{/await}
]]
local _s_catch_block = [[
	{:catch error}
		<!-- promise was rejected -->
]]
local _s_each_index_block = [[
	{#each ${1:items} as ${2:item},${3:i}}
		${4: <!-- content here -->}
	{/each}
]]
local _s_class_short = [[
	class:${1:name}}
]]
local _s_on_event_inline = [[
	on:${1:click}="{() => ${2:count += 1}}"
]]
local _s_await_short_block = [[
	{#await ${1:promise} then ${2:value}}
		<!-- promise was fulfilled -->
	{/await}
]]
local _s_expression = [[
	{${1:expression}}
]]
local _s_transition_events = [[
	on:${1|introstart,introend,outrostart,outroend|}="{() => status = '${1|introstart,introend,outrostart,outroend|}'}"
]]
local _s_window = [[
	<svelte:window />
]]
local _s_if_else_block = [[
	{#if ${1:condition}}
		${2: <!-- content here -->}
	{:else}
		${3: <!-- else content here -->}
	{/if}
]]
local _s_transition_all = [[
	${1|transition,in,out|}:${2:name}|${3:local}={${4:params}}
]]
local _s_use = [[
	use:action
]]
local _s_use_parameters = [[
	use:action={${1:parameters}}
]]
local _s_else_if_block = [[
	{:else if ${1: otherCondition}}
		${2: <!-- else if content here -->}
]]
local _s_if_else_if_block = [[
	{#if ${1:condition}}
		${2: <!-- content here -->}
	{:else if ${3: otherCondition}}
		${4: <!-- else if content here -->}
	{:else}
		${5: <!-- else content here -->}
	{/if}
]]
local _s_each_key_block = [[
	{#each ${1:items} as ${2:item},(${3:key})}
		${4: <!-- content here -->}
	{/each}
]]
local _s_transition = [[
	${1|transition,in,out|}:${2:name}
]]
local _s_bind_this = [[
	bind:this={${1:dom_node}}
]]
local _s_bind = [[
	bind:${1:property}
]]
local _s_each_else_block = [[
	{#each ${1:items} as ${2:item}}
		${3: <!-- content here -->}
	{:else}
		${4: <!-- empty list -->}
	{/each}
]]
local _s_transition_local = [[
	${1|transition,in,out|}:${2:name}|${3:local}
]]
local _s_then_block = [[
	{:then ${1:value}}
		<!-- promise was fulfilled -->
]]
local _s_bind_video = [[
	<video
	src={${1:clip}}
	bind:${2:duration}
	bind:${3:buffered}
	bind:${4:played}
	bind:${5:seekable}
	bind:${6:seeking}
	bind:${7:ended}
	bind:${8:currentTime}
	bind:${9:playbackRate}
	bind:${10:paused}
	bind:${11:volume}
	bind:${12:muted}
	bind:${13:videoWidth}
	bind:${14:videoHeight}
	></video>
]]
local _s_each_index_key_block = [[
	{#each ${1:items} as ${2:item},i (${3:key})}
		${4: <!-- content here -->}
	{/each}
]]
local _s_modifier = [[
	|${1|preventDefault,stopPropagation,passive,capture,once|}
]]
local _s_on_event_foward = [[
	on:${1:eventname}
]]
local _s_if_block = [[
	{#if ${1:condition}}
		${2: <!-- content here -->}
	{/if}
]]
function _svelte.snippets()
	return {
		s_on_event_modifiers = _s_on_event_modifiers,
		s_self = _s_self,
		s_options = _s_options,
		s_await_then_block = _s_await_then_block,
		s_on_event = _s_on_event,
		s_body = _s_body,
		s_bind_property = _s_bind_property,
		s_bind_group = _s_bind_group,
		s_bind_audio = _s_bind_audio,
		s_expression_html = _s_expression_html,
		s_head = _s_head,
		s_bind_block_level = _s_bind_block_level,
		s_bind_media_elements = _s_bind_media_elements,
		s_window_bind = _s_window_bind,
		s_transition_params = _s_transition_params,
		s_else_block = _s_else_block,
		s_script_context = _s_script_context,
		s_each_block = _s_each_block,
		s_component = _s_component,
		s_style = _s_style,
		s_component_format = _s_component_format,
		s_expression_debug = _s_expression_debug,
		s_self_prop = _s_self_prop,
		s_slot_prop = _s_slot_prop,
		s_class = _s_class,
		s_slot_name = _s_slot_name,
		s_animate = _s_animate,
		s_script = _s_script,
		s_slot = _s_slot,
		s_await_catch_block = _s_await_catch_block,
		s_catch_block = _s_catch_block,
		s_each_index_block = _s_each_index_block,
		s_class_short = _s_class_short,
		s_on_event_inline = _s_on_event_inline,
		s_await_short_block = _s_await_short_block,
		s_expression = _s_expression,
		s_transition_events = _s_transition_events,
		s_window = _s_window,
		s_if_else_block = _s_if_else_block,
		s_transition_all = _s_transition_all,
		s_use = _s_use,
		s_use_parameters = _s_use_parameters,
		s_else_if_block = _s_else_if_block,
		s_if_else_if_block = _s_if_else_if_block,
		s_each_key_block = _s_each_key_block,
		s_transition = _s_transition,
		s_bind_this = _s_bind_this,
		s_bind = _s_bind,
		s_each_else_block = _s_each_else_block,
		s_transition_local = _s_transition_local,
		s_then_block = _s_then_block,
		s_bind_video = _s_bind_video,
		s_each_index_key_block = _s_each_index_key_block,
		s_modifier = _s_modifier,
		s_on_event_foward = _s_on_event_foward,
		s_if_block = _s_if_block,
	}
end

return _svelte