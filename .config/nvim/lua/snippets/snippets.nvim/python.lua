local _python = {}
local U = require'snippets.utils'

local _main = [[
#!/usr/bin/env python3

# Copyright (C) Stefan Zanko 2021

${0}

if __name__ == "__main__":
    main()
]]

local _def = [[
def ${1}(${2}) -> ${3}:
    """${4}"""
    ${0}
]]

local _class = [[
class ${1}:
    """${2}"""

    def __init__(self) -> None:
        ${0}
]]

local _singleton = [[
from typing import Optional

class ${1}:
	"""${2:Description}"""
	__instance:Optional["${1}"] = None

	@staticmethod
	def get_instance() -> Optional["${1}"]:
		if ${1}.__instance is None:
			${1}()
		return ${1}.__instance

	def __init__(self) -> None:
		${0}
		if ${1}.__instance is not None:
			raise Exception("{1} is a singleton! use the get_instance method instead")
		${1}.__instance = self
]]

function _python.snippets()
  return {
    main = _main,
    def = _def,
    class = _class,
    singleton = _singleton
  }
end

return _python
