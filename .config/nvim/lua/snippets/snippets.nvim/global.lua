local _global = {}
local U = require'snippets.utils'

local _copyright = U.force_comment [[Copyright (C) Stefan Zanko ${=os.date("%Y")}]];


function _global.snippets()
  return {
    copyright = _copyright,
  }
end

return _global
