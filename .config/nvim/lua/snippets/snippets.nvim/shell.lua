local _shell = {}
local pkgbuild_boilerplate=[[
# Maintainer: SZanko <szanko at protonmail dot com>


pkgname=<++>
pkgver=<++>
pkgrel=<++>
pkgdesc="<++>"
arch=('<++>')
license=('<++>')
makedepends=('<++>')
depends=(
    '<++>'
  )
source=("${url}/archive${pkgver}.tar.gz")
sha256sums=('<++>')

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  <++>
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  <++>
  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
]];

function _shell.snippets()
	return {
	pkgbuild= pkgbuild_boilerplate;
  }
end

return _shell
