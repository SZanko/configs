local _typescript = {}
local U = require'snippets.utils'

local __region = [[
	//#region $0
]]
local _async_arrow_function = [[
	async (${1:params}:${2:type}) => {
		$0
	}
]]
local _throw = [[
	throw new Error("$1");
	$0
]]
local _async_function = [[
	async function ${1:name}(${2:params}:${3:type}) {
		$0
	}
]]
local _forin = [[
	for (const ${1:key} in ${2:object}) {
		if (Object.prototype.hasOwnProperty.call(${2:object}, ${1:key})) {
			const ${3:element} = ${2:object}[${1:key}];
			$0
		}
	}
]]
local _newpromise = [[
	new Promise<${1:void}>((resolve, reject) => {
		$0
	})
]]
local _switch = [[
	switch (${1:key}) {
		case ${2:value}:
			$0
			break;
	
		default:
			break;
	}
]]
local _if = [[
	if (${1:condition}) {
		$0
	}
]]
local _function = [[
	function ${1:name}(${2:params}:${3:type}) {
		$0
	}
]]
local _error = [[
	console.error($1);
	$0
]]
local __endregion = [[
	//#endregion
]]
local _forawaitof = [[
	for await (const ${1:iterator} of ${2:object}) {
		$0
	}
]]
local _ctor = [[
	/**
	 *
	 */
	constructor() {
		super();
		$0
	}
]]
local _private_method = [[
	private ${1:name}() {
		$0
	}
]]
local _settimeout = [[
	setTimeout(() => {
		$0
	}, ${1:timeout});
]]
local _set = [[
	
	public set ${1:value}(v : ${2:string}) {
		this.$3 = v;
	}
	
]]
local _prop = [[
	
	private _${1:value} : ${2:string};
	public get ${1:value}() : ${2:string} {
		return this._${1:value};
	}
	public set ${1:value}(v : ${2:string}) {
		this._${1:value} = v;
	}
	
]]
local _trycatch = [[
	try {
		$0
	} catch (${1:error}) {
		
	}
]]
local _dowhile = [[
	do {
		$0
	} while (${1:condition});
]]
local _class = [[
	class ${1:name} {
		constructor(${2:parameters}) {
			$0
		}
	}
]]
local _new = [[
	const ${1:name} = new ${2:type}(${3:arguments});$0
]]
local _public_method = [[
	/**
	 * ${1:name}
	 */
	public ${1:name}() {
		$0
	}
]]
local _warn = [[
	console.warn($1);
	$0
]]
local _ifelse = [[
	if (${1:condition}) {
		$0
	} else {
		
	}
]]
local _forof = [[
	for (const ${1:iterator} of ${2:object}) {
		$0
	}
]]
local _import_statement = [[
	import { $0 } from "${1:module}";
]]
local _get = [[
	
	public get ${1:value}() : ${2:string} {
		${3:return $0}
	}
	
]]
local _log = [[
	console.log($1);
	$0
]]
local _while = [[
	while (${1:condition}) {
		$0
	}
]]
local _for = [[
	for (let ${1:index} = 0; ${1:index} < ${2:array}.length; ${1:index}++) {
		const ${3:element} = ${2:array}[${1:index}];
		$0
	}
]]
local _ref = [[
	/// <reference path="$1" />
	$0
]]
local _foreach_ = [[
	${1:array}.forEach(${2:element} => {
		$0
	});
]]
function _typescript.snippets()
	return {
		_region = __region,
		async_arrow_function = _async_arrow_function,
		throw = _throw,
		async_function = _async_function,
		forin = _forin,
		newpromise = _newpromise,
		switch = _switch,
		["if"] = _if,
		["function"] = _function,
		error = _error,
		_endregion = __endregion,
		forawaitof = _forawaitof,
		ctor = _ctor,
		private_method = _private_method,
		settimeout = _settimeout,
		set = _set,
		prop = _prop,
		trycatch = _trycatch,
		dowhile = _dowhile,
		class = _class,
		new = _new,
		public_method = _public_method,
		warn = _warn,
		ifelse = _ifelse,
		forof = _forof,
		import_statement = _import_statement,
		get = _get,
		log = _log,
		["while "]= _while,
		["for"] = _for,
		ref = _ref,
		["foreach =>"] = _foreach_,
	}
end

return _typescript
