local map = vim.api.nvim_set_keymap
local cmd = vim.cmd
local dap = require "dap"

map("n", "<A-s>", ":vs<CR>", {noremap = true})
map("n", ",,", '<Esc>/<++><Enter>"_c4l', {noremap = true})
map("n", "<C-l>", ":vertical resize +3<CR>", {noremap = true})
map("n", "<C-h>", ":vertical resize -3<CR>", {noremap = true})
map("i", "<S-Tab>", "<C-d>", {noremap = true, silent = true})

-- basic autopair
map("i", '"', '""<left>', {noremap = true, silent = true})
map("i", "`", "``<left>", {noremap = true, silent = true})
map("i", "'", "''<left>", {noremap = true, silent = true})
map("i", "(", "()<left>", {noremap = true, silent = true})
map("i", "[", "[]<left>", {noremap = true, silent = true})
map("i", "{", "{}<left>", {noremap = true, silent = true})
map("i", "{<CR>", "{<CR}<ESC>0", {noremap = true, silent = true})
map("i", "{;<CR>", "{<CR};<ESC>0", {noremap = true, silent = true})

-- Plugins
-- NERDTree
map("n", "<A-n>", ":NERDTreeToggle<CR>", {noremap = true, silent = true})

-- Telescope
map("n", "<A-f>", ":Telescope live_grep<CR>", {noremap = true})
map("n", "<A-e>", ":Telescope find_files<CR>", {noremap = true})

-- Latex Preview
-- map("n", "<F5>", ":LLPStartPreview <CR>", {noremap = true})

-- Snippets
-- cmd([[inoremap <c-k> <cmd>lua return require'snippets'.expand_or_advance(1)<CR>]])
-- cmd([[inoremap <c-j> <cmd>lua return require'snippets'.expand_or_advance(-1)<CR>]])

-- autocomplete with Tab nvim-compe
-- map('i', '<Tab>', 'v:lua.tab_complete()', {expr = true})
-- map('s', '<Tab>', 'v:lua.tab_complete()', {expr = true})
-- map('i', '<S-Tab>', 'v:lua.s_tab_complete()', {expr = true})
-- map('s', '<S-Tab>', 'v:lua.s_tab_complete()', {expr = true})

-- Mappings.
-- See `:help vim.lsp.*` for documentation on any of the below functions
local bufopts = {noremap = true, silent = true, buffer = bufnr}
vim.keymap.set("n", "gD", vim.lsp.buf.declaration, bufopts)
vim.keymap.set("n", "gd", vim.lsp.buf.definition, bufopts)
vim.keymap.set("n", "K", vim.lsp.buf.hover, bufopts)
vim.keymap.set("n", "gi", vim.lsp.buf.implementation, bufopts)
vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, bufopts)
vim.keymap.set("n", "<space>wa", vim.lsp.buf.add_workspace_folder, bufopts)
vim.keymap.set("n", "<space>wr", vim.lsp.buf.remove_workspace_folder, bufopts)
vim.keymap.set(
  "n",
  "<space>wl",
  function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end,
  bufopts
)
vim.keymap.set("n", "<space>D", vim.lsp.buf.type_definition, bufopts)
vim.keymap.set("n", "<space>rn", vim.lsp.buf.rename, bufopts)
vim.keymap.set("n", "<space>ca", vim.lsp.buf.code_action, bufopts)
vim.keymap.set("n", "gr", vim.lsp.buf.references, bufopts)
vim.keymap.set("n", "<space>f", vim.lsp.buf.format, bufopts)

-- Debugging"

map("n", "<space>b", ":lua require'dap'.toggle_breakpoint()<CR>", {noremap = true, silent = true})
map("n", "<F5>", ":lua require'dap'.continue()<CR>", {noremap = true, silent = true})

map("n", "<F7>", ":lua require'dap'.step_into()<CR>", {noremap = true, silent = true})
map("n", "<S-F7>", ":lua require'dap'.step_out()<CR>", {noremap = true, silent = true})
map("n", "<F8>", ":lua require'dap'.step_over()<CR>", {noremap = true, silent = true})
map("n", "<space>dr", ":lua require'dap'.repl.open()<CR>", {noremap = true, silent = true})
map("n", "<space>dl", ":lua require'dap'.run_last()<CR>", {noremap = true, silent = true})
