--local lsp_installer = require("nvim-lsp-installer")
require("mason").setup()
require("mason-lspconfig").setup{
  ensure_installed=servers
}

-- default servers
local servers = {
  "bashls",
  "cssls",
  "deno",
  "html",
  "jdtls",
  "jedi_language_server",
  "jsonls",
  "pylsp",
  "pyright",
  "sqls",
  "sumneko_lua",
  "texlab",
  "yamlls",
  "zeta_note"
}


require("mason-lspconfig").setup_handlers {
  -- The first entry (without a key) will be the default handler
  -- and will be called for each installed server that doesn't have
  -- a dedicated handler.
  function(server_name) -- default handler (optional)
    require("lspconfig")[server_name].setup {}
  end

}

--for _, name in pairs(servers) do
--  --local server_is_found, server = mason.get_server(name)
--  if server_is_found and not server:is_installed() then
--    print("Installing " .. name)
--    server:install()
--  end
--end

---- Register a handler that will be called for each installed server when it's ready (i.e. when installation is finished
---- or if the server is already installed).
--lsp_installer.on_server_ready(
--  function(server)
--    local opts = {}
--
--    -- (optional) Customize the options passed to the server
--    -- if server.name == "tsserver" then
--    --     opts.root_dir = function() ... end
--    -- end
--
--    -- This setup() function will take the provided server configuration and decorate it with the necessary properties
--    -- before passing it onwards to lspconfig.
--    -- Refer to https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
--    if server.name == "rust_analyzer" then
--      -- Initialize the LSP via rust-tools instead
--      require("rust-tools").setup {
--        -- The "server" property provided in rust-tools setup function are the
--        -- settings rust-tools will provide to lspconfig during init.            --
--        -- We merge the necessary settings from nvim-lsp-installer (server:get_default_options())
--        -- with the user's own settings (opts).
--        server = vim.tbl_deep_extend("force", server:get_default_options(), opts)
--      }
--      server:attach_buffers()
--      -- Only if standalone support is needed
--      require("rust-tools").start_standalone_if_required()
--    else
--      server:setup(opts)
--    end
--  end
--)
---- Programming
--local nvim_lsp = require('lspconfig')
--
--
---- html
--local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
--capabilities.textDocument.completion.completionItem.snippetSupport = true
--
--require'lspconfig'.html.setup {
--	--  on_attach=require'completion'.on_attach,
--	capabilities = capabilities,
--	cmd = { "vscode-html-languageserver", "--stdio" }
--}
--
---- Javascript / Typescript
--require'lspconfig'.svelte.setup{
--	--on_attach=require'completion'.on_attach
--}
---- require'lspconfig'.denols.setup{on_attach=require'completion'.on_attach}
--require'lspconfig'.tsserver.setup{
--	--on_attach=require'completion'.on_attach
--}
--
--
---- python
--require'lspconfig'.jedi_language_server.setup{
--	--on_attach=require'completion'.on_attach
--}
--require'lspconfig'.pyright.setup{
--	--on_attach=require'completion'.on_attach
--}
--
--
--require'lspconfig'.bashls.setup{
--	--on_attach=require'completion'.on_attach
--}
--require'lspconfig'.dartls.setup{
--	--on_attach=require'completion'.on_attach
--}
----require'lspconfig'.diagnosticls.setup{}
--require'lspconfig'.gopls.setup{
--	--on_attach=require'completion'.on_attach
--}
--require'lspconfig'.graphql.setup{
--	--on_attach=require'completion'.on_attach
--}
--
---- lua
---- require'lsp/lua'
--
---- java
---- eclipse ls https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#jdtls
---- require'lsp/jdtls'
---- java language server
---- require'lsp/java'
--
---- Configurations other stuff
--
---- sqls
--require'lspconfig'.sqls.setup{
--	--on_attach=require'completion'.on_attach
--}
---- docker
--require'lspconfig'.dockerls.setup{
--	--on_attach=require'completion'.on_attach
--}
---- latex
--require'lspconfig'.texlab.setup{
--	--on_attach=require'completion'.on_attach
--}
---- yaml
--require'lspconfig'.yamlls.setup{
--	--on_attach=require'completion'.on_attach
--}
---- vim
--require'lspconfig'.vimls.setup{
--	--on_attach=require'completion'.on_attach
--}
