local cmd = vim.cmd
local g = vim.g         -- global variables


g.completion_enable_snippet = 'snippets.nvim'
cmd([[inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"]])
cmd([[inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"]])
