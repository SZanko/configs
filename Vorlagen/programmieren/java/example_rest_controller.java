package <++>;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author	<++>
 * @version 1.0
 * @since	<++>
 */
@RestController
@RequestMapping(path="<++>")
public class <++> {

	/**
	 * The Repository which is used by this Controller
	 */
	@Autowired
	private <++>Repository <++>Repository;

	/**
	 * Return all <++> in the Repository
	 *
	 * @return List<<++>> as Json
	 */
	@GetMapping
	@Async
	public @ResponseBody Iterable<<++>>
	getAll<++>(){
		return <++>Repository.findAll();
	}

	/**
	 * Gets one Patient by their Id
	 * at the url /api/<++>/{id}
	 *
	 * @param id of the wanted Patient
	 * @return Patient with the given id as a String
	 */
	@GetMapping("/{id}")
	@Async
	public ResponseEntity<<++>>
	get<++>(@PathVariable String id){
		<++> <++> = <++>Repository.findById(id).get();

		if(<++> == null) {

			return ResponseEntity.notFound().build();

		}

		return ResponseEntity.ok().body(<++>); 
	}

	/**
	 * Saves one <++> to the Database
	 *
	 * @param <++> the full <++> as a Json
	 * @return Saved <++>
	 */
	@PostMapping
	@Async
	public <++>
	new<++>(@RequestBody <++> <++>){
		return <++>Repository.save(<++>);
	}

	/**
	 * Updates a Patient by their Id
	 * at the url /api/<++>/{id}
	 *
	 * @param new<++> the full <++> as Json
	 * @param id the Id as a String of the <++> which will be updated
	 */
	@PutMapping("/{id}")
	@Async
	public <++>
	replace<++>(@RequestBody <++> <++>, @PathVariable String id){
		return <++>Repository.findById(id).map(
				<++> ->{
					<++>.setProperty() //edit this Line
						return <++>Repository.save(<++>);
				}
				).orElseGet(()->{
			<++>.setId(id);

			return <++>Repository.save(newpatient);
		});
	}

	/**
	 * Deletes a <++> by their Id
	 * at the url /api/<++>/{id}
	 *
	 * @param id the Id as a String of the <++> which will be deleted
	 */
	@DeleteMapping("/{id}")
	@Async
	public void
	delete<++>(@PathVariable String id){
		<++>Repository.deleteById(id);
	}



}
