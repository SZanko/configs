#!/bin/sh

export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"
export PATH="$PATH":"$HOME/.cache/pub/bin"
export PATH="$PATH":"$HOME/.local/share/cargo/bin"

OS="$(uname)"
case $OS in
  'Linux')
    OS='Linux'
    export TERM="st"
    export TERMINAL="st"
    export SUDO_ASKPASS="$HOME/.local/bin/dmenupass"
    #export JAVA_HOME=/usr/lib/jvm/default
    eval "$(gnome-keyring-daemon --start)"
    export SSH_AUTH_SOCK
    export VIEWER="nsxiv"
    exec dbus-update-activation-environment --systemd DBUS_SESSION_BUS_ADDRESS DISPLAY XAUTHORITY &
    export JDTLS_JVM_ARGS="-javaagent:/usr/lib/lombok-common/lombok.jar"
    ;;
  'FreeBSD')
    OS='FreeBSD'
    ;;
  'WindowsNT')
    OS='Windows'
    ;;
  'Darwin') 
    OS='Mac'
    ;;
  'SunOS')
    OS='Solaris'
    ;;
  'AIX') ;;
  *) ;;
esac

export XDG_DATA_DIRS="$XDG_DATA_DIRS:/var/lib/flatpak/exports/share:$HOME/.local/share/flatpak/exports/share"
export QT_QPA_PLATFORMTHEME="qt5ct"
export QT_QPA_PLATFORM=xcb
export EDITOR="nvim"
export FILE="lf"
export READER="zathura"
export LIBBY_VIEWER="zathura"
export BROWSER="firefox-developer-edition"
#export BROWSER="firefox"
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GPG_TTY="$(tty)"
export LOCATION="Wien"
#export DOCKER_HOST="unix://$XDG_RUNTIME_DIR/podman/podman.sock"
export QT_QPA_PLATFORMTHEME="qt5ct"
export ICON_THEME="Papirus-Dark"


# ~/ Clean-up:
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export GRADLE_USER_HOME="$XDG_DATA_HOME"/gradle
export IPYTHONDIR="$XDG_CONFIG_HOME"/jupyter

#Android
export ANDROID_SDK_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/android"
export ANDROID_AVD_HOME="$XDG_DATA_HOME"/android/
export ANDROID_EMULATOR_HOME="$XDG_DATA_HOME"/android/
export ADB_VENDOR_KEY="$XDG_CONFIG_HOME"/android
export CHROME_EXECUTABLE=/usr/bin/env chromium

#Programmieren
export NODE_REPL_HISTORY="$XDG_DATA_HOME"/node_repl_history
#export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME"/jupyter
export GOPATH="$XDG_DATA_HOME"/go
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv
export CCACHE_DIR="$XDG_CACHE_HOME"/ccache
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export PYENV_ROOT="$XDG_DATA_HOME"/pyenv
export PYLINTHOME="$XDG_DATA_HOME"/pylint
export RUSTC_WRAPPER=sccache
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export WORKSPACE="${XDG_DATA_HOME}:-$HOME/workspace"
export CHROOT="${HOME}/Documents/build/AUR/BuildRoot"

#Datenbanken
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history

#Package_Management
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export NUGET_PACKAGES="$XDG_CACHE_HOME"/NuGetPackages
export PUB_CACHE="$XDG_CACHE_HOME"/pub
export CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/cargo"


#Programme
export LESSKEY="$XDG_CONFIG_HOME"/less/lesskey
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export OCTAVE_SITE_INITFILE="$XDG_CONFIG_HOME/octave/octaverc"
export OCTAVE_HISTFILE="$XDG_CACHE_HOME/octave-hsts"
export IDE_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/ide/"
export WINEPREFIX="${XDG_DATA_HOME:-$HOME/.local/share}/wineprefixes/default"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass
export _JAVA_AWT_WM_NONREPARENTING=1
export MYSQL_HISTFILE="$XDG_DATA_HOME/mysql_history"
export SAL_ENABLESKIA=1
export AL_USE_VCLPLUGIN=gen
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export PYTHON_KEYRING_BACKEND=keyring.backends.fail.Keyring


# This is the list for lf icons:
export LF_ICONS="di=📁:\
fi=📃:\
tw=🤝:\
ow=📂:\
ln=⛓:\
or=❌:\
ex=🎯:\
*.txt=✍:\
*.mom=✍:\
*.me=✍:\
*.ms=✍:\
*.png=🖼:\
*.ico=🖼:\
*.jpg=📸:\
*.jpeg=📸:\
*.gif=🖼:\
*.svg=🗺:\
*.xcf=🖌:\
*.html=🌎:\
*.xml=📰:\
*.gpg=🔒:\
*.css=🎨:\
*.pdf=📚:\
*.djvu=📚:\
*.epub=📚:\
*.csv=📓:\
*.xlsx=📓:\
*.tex=📜:\
*.md=📘:\
*.r=📊:\
*.R=📊:\
*.rmd=📊:\
*.Rmd=📊:\
*.mp3=🎵:\
*.opus=🎵:\
*.ogg=🎵:\
*.m4a=🎵:\
*.flac=🎼:\
*.mkv=🎥:\
*.mp4=🎥:\
*.webm=🎥:\
*.mpeg=🎥:\
*.avi=🎥:\
*.zip=📦:\
*.rar=📦:\
*.7z=📦:\
*.tar.gz=📦:\
*.z64=🎮:\
*.v64=🎮:\
*.n64=🎮:\
*.1=ℹ:\
*.nfo=ℹ:\
*.info=ℹ:\
*.log=📙:\
*.iso=📀:\
*.img=📀:\
*.bib=🎓:\
*.ged=👪:\
*.part=💔:\
*.torrent=🔽:\
"
. "$XDG_CACHE_HOME/passwd"


#startx &
# Start graphical server on tty1 if not already running.
#[ "$(tty)" = "/dev/tty1" ] && ! ps -e | grep -qw Xorg && exec startx
